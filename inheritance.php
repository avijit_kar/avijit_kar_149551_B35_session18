<?php
class BITM
{
    public $window;
    public $door;
    public $chair;
    public $table;
    public $whiteboard;

    public function coolTheAir()
    {
     echo "I'm cooling the air";
    }

    public function compute()
    {
        echo "I am computing";
    }
    public function show()
    {
        //echo "I am showing";
        echo $this->window."<br>";
        echo $this->door."<br>";
        echo $this->table."<br>";
        echo $this->chair."<br>";
        echo $this->whiteboard."<br>";
    }
    public function setChair($chair)
    {
        $this->chair=$chair;
    }

    public function setDoor($door)
    {
        $this->door = $door;
    }

    public function setWindow($window)
    {
        $this->window = $window;
    }

    public function setWhiteboard($whiteboard)
    {
        $this->whiteboard = $whiteboard;
    }

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function parentDosomething()
    {
        echo "Do something";
    }
    public function setAll()
{
$this->setChair("Im a Chair");
$this->setTable("Im a table");
$this->setDoor("Im a Door");
$this->setWindow("Im a Window");
$this->setWhiteboard("Im a whiteboard");
}
}
$objBITMatCtg =new BITM;
$objBITMatCtg->setAll();
//$objBITMatCtg->setChair("Im a Chair");
//$objBITMatCtg->setTable("Im a table");
//$objBITMatCtg->setDoor("Im a Door");
//$objBITMatCtg->setWindow("Im a Window");
//$objBITMatCtg->setWhiteboard("Im a whiteboard");
$objBITMatCtg->show();

class BITM_Lab402 extends BITM{
   public $childChair;

    public function setChildChair($chair)
    {
        parent::setChair($chair);
    }



}
$objBITM_Lab=new BITM_Lab402();
$objBITM_Lab->setWindow("this is a window");
$objBITM_Lab->setChair("this is a charir");
$objBITM_Lab->setDoor("this is a door");
$objBITM_Lab->setWhiteboard("this a whiteboard");
$objBITM_Lab->show();
$objBITM_Lab->setChildChair("ami child");
echo $objBITM_Lab->chair;